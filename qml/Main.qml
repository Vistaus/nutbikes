/*
 * Copyright (C) 2021  s60w79
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * NUTbikes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Popups 1.3
import "jslib/getlog.js" as ApiLog
MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'nutbikes.s60w79'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    property string authkey : ""
    property var data : {}
    property var collections :[]
    property string toreturn;
    Settings{
        id:sets
        //preset api key, will be updated automatically in cases it should be changed.
        property string apikey : "xEsFhYZR5jpO1Ug1"
        //number, necassary for log in
        property string number : ""
        //pin (6 Digits) necassary for log in
        property string pin : ""
        property bool inform : true
    }
AdaptivePageLayout {
    Component.onCompleted:{
        
        ApiLog.keyget(function(sucess, key){
            if(sucess){
            var apik = JSON.parse(key)["apiKey"];
            if(apik != null && apik != ""){
                //update api key
                sets.apikey = apik;
                
            }
            }
        });
    }
    anchors.fill: parent
    primaryPage: curbikes
    Page {
         Component {
         id: startdia
         Dialog {
             
             id: startdia
             title: i18n.tr("Welcome")
             text: i18n.tr("This is the first version of the Nextbike Client for UT.\nBasic functions are tested-but only in one City.\nIn case of Errors open an issue (button below). If the app works just fine in your city, tell us (2nd Button below)\nIf any critical errors occur (lock doesn't close,...) tell the official Nextbike support (via telephone) and pretend to not know anything about technique(;\nYOUR HELP IS NEEDED:\nNextbike provides Services in 28 Countries, so we need more translations. Use the form below to offer your help.\nAs allways, developers are welcome too.")
             Button {
                 text: i18n.tr("Report Errors")
                 color: UbuntuColors.red
                 onClicked: Qt.openUrlExternally("https://gitlab.com/S60W79/nutbikes/-/issues")
             }
             Button {
                 text: i18n.tr("App works fine in...")
                 onClicked: Qt.openUrlExternally("https://gitlab.com/S60W79/nutbikes/-/issues/2")
             }
             Button {
                 text: i18n.tr("Help translating")
                 onClicked: Qt.openUrlExternally("https://gitlab.com/S60W79/nutbikes/-/issues/1")
             }
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.green
                 onClicked: PopupUtils.close(startdia)
             }
             Button {
                 text: i18n.tr("close&not show again")
                 
                 onClicked:{PopupUtils.close(startdia)
                     sets.inform = false;
                 }
             }
         }
    }
        anchors.fill: parent
        id:curbikes
        Component.onCompleted:{
            if(sets.inform){
            PopupUtils.open(startdia);
        } 
        }
        
        header: PageHeader {
            id: header
            title: i18n.tr('Next UT bikes')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("NUTbikes")
								iconSource : Qt.resolvedUrl("../assets/logo.svg")
								onTriggered : {
                                    curbikes.pageStack.addPageToNextColumn(curbikes, Qt.resolvedUrl("Information.qml"))
								}
							}
						]
					}
					
                    trailingActionBar  { 
						numberOfSlots: 2
						actions : [
							Action {
								text : i18n.tr("Account information")
								iconName : "info"
								onTriggered : {
									curbikes.pageStack.addPageToNextColumn(curbikes, ainfo);
								}
							},
							Action {
								text : i18n.tr("Account")
								iconName : "settings"
								onTriggered : {
									curbikes.pageStack.addPageToNextColumn(curbikes, account);
                                    //curbikes.pageStack.removePages(curbikes);
								}
							}
						]
					}
        }
        
        //pseudo labels, just for correct translation, ALL invisible...
        Label{
            id:text1
            visible:false;
            text: i18n.tr('Duration of rental:')
        }
        
        Label{
            id:text2
            visible:false;
            text: i18n.tr("please fill out the fields.\n\nIf you return the bike at a station, fill in the station's ID,\n if not fill in the Description of the place you are now\n into 'Place of return'")
        }
        
        //end of pseudo labels
Rectangle {
    color: Qt.rgba(0.27, 0.27, 0.27);
    id:mainbike
             anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
    
    width: 200; height: 200
    ListModel {
        id: bikes
        
    }
    Component {
                
        id: fruitDelegate
        ListItem {
            leadingActions: ListItemActions {
        actions: [
            Action {
                iconName: "redo"
                onTriggered:{
                    root.toreturn = bikenr;
                    curbikes.pageStack.addPageToNextColumn(curbikes, retdia);
                    
                }
            }
        ]
    }

        ListItemLayout {
        id: layout
        title.text: i18n.tr("Bike number: "+bikenr)
        subtitle.text: counter
    }
        }
    }
    ListView {
        anchors.fill: parent
        model: bikes
        delegate: fruitDelegate
        
        Timer {
    id: timer
    interval: 15000; repeat: true
    running: true
    triggeredOnStart: true
    onTriggered: {
       
        
        bikes.clear();
        appendhelper();
                 if(root.authkey == "" && sets.number != ""){
                ApiLog.logdata(sets.apikey, sets.number, sets.pin, function(sucess, sheet){
                    if(sucess){
                        root.data=sheet;
                        root.authkey=sheet["user"]["loginkey"];
                        
                    
                        var usr = sheet["user"];
                        var act = "";
                        if(usr["active"]){act="YES";}else{act="NO"}
                        status1List.visible=true;
                        status2List.visible=false;
                        numberList.text=usr["mobile"];
                        userList.text = usr["screen_name"];
                        activList.text = act;
                        creditList.text = usr["credits"]+usr["currency"];
                        minutesList.text = usr["free_seconds"]/60;
                        
                    }
                });
            }else{
                    //in the first login numer+pwd aren't available. Showing the error message would irritate the user.
                    
                    if(root.data !={}){
                      var sheet=root.data;
                        root.authkey=sheet["user"]["loginkey"];
                        
                    
                        var usr = sheet["user"];
                        var act = "";
                        if(usr["active"]){act="YES";}else{act="NO"}
                        status1List.visible=true;
                        status2List.visible=false;
                        numberList.text=usr["mobile"];
                        userList.text = usr["screen_name"];
                        activList.text = act;
                        creditList.text = usr["credits"]+usr["currency"];
                        minutesList.text = usr["free_seconds"]/60;
                        
                      
                    }else{
                        status1List.visible=false;
                        status2List.visible=true;
                    }
        }
         ApiLog.current(sets.apikey, root.authkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
        });
        
}
}
    }
}
        Label {
            id:mainarea
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            text: i18n.tr('Swipe up to rent a bike')

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
        }
        
        	BottomEdge {
    id: bottomEdge
    height: parent.height
    hint.text: i18n.tr("Rent a bike")
    contentComponent: Rectangle {
        
               Component {
         id: rentocc
         Dialog {
             
             id: rentdia
             title: i18n.tr("Occupied")
             text: i18n.tr("No bike has been rent.\nThe bike has been locked by another user.")
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.yellow
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
         Component {
         id: rentno
         Dialog {
             
             id: rentdia
             title: i18n.tr("Failed")
             text: i18n.tr("No bike has been rent.\nPlease check if this was the correct number and you are logged in correctly.")
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.orange
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
    Component {
         id: rentyes
         Dialog {
             
             id: rentdia
             title: i18n.tr("Bike Rent")
             text: i18n.tr("The bike should now be available.\nHave a good and joyfull trip.")
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.green
                 onClicked: PopupUtils.close(rentdia)
             }
         }
    }
        width: bottomEdge.width
        height: bottomEdge.height
         color: Qt.rgba(0.27, 0.27, 0.27);
        PageHeader {
            id:header3
            title: i18n.tr("Rent Bikes");
        }
        Row {
            Component.onCompleted:{
                if(root.authkey == "" && sets.number != ""){
                ApiLog.logdata(sets.apikey, sets.number, sets.pin, function(sucess, sheet){
                    if(sucess){
                        root.authkey=sheet["user"]["loginkey"];
                        root.data=sheet;
                    }
                
                });
                }
            }
                
                    
            id:mainarea2
            anchors {
                top: header3.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins:units.gu(0.5)
            }
            spacing:units.gu(0.2)
            
            TextField {
			id:bikenr
			property bool isLegitimate : true
			property bool isURL : false
			placeholderText: i18n.tr("Bike Number (Below QR Code)")
            }
            Button {
                 iconName: "keyboard-enter"
                 color: UbuntuColors.green
                 width:units.gu(5)
                 onClicked:{
                     var informed = false;
                     ApiLog.bike(sets.apikey, bikenr.text, root.authkey, function(sucess, answer){
                         if(sucess){
                             if(!informed){
                             PopupUtils.open(rentyes);
                             informed = true;
                             ApiLog.current(sets.apikey, root.authkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             
                             }
                             
                         }else{
                            //rentdia.text="...was NOT sucessfull."
                             if(!informed){
                            if(answer == 423){
                                PopupUtils.open(rentocc);
                            }else{
                             PopupUtils.open(rentno);
                            }
                             informed = true;
                             ApiLog.current(sets.apikey, root.authkey, function(subsucess, answer){
                                 if(subsucess){
                                     root.collections = answer;
                             }
                                    });
                             }
                         }
                     });
                 }
             }
                
        }
    }
}
    }
    Page{
        
            Component {
         id: yes
         Dialog {
             
             id: yes
             title: i18n.tr("LOGGED IN")
             text: i18n.tr("You logged in successfully. You now can access your account data.")
             Button {
                 text: "OK"
                 color: UbuntuColors.green
                 onClicked:{
                     account.pageStack.removePages(account);
                     PopupUtils.close(yes);
                 }
             }
         }
    }
                Component {
         id: no
         Dialog {
             
             id: no
             title: i18n.tr("ERROR")
            text:i18n.tr("The login wasn't sucessfull, Likely because of a wrong username or password.\n Please try another one.")
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.orange
                 onClicked: PopupUtils.close(no)
             }
         }
    }
        //Account & settings
        anchors.fill: parent
        id:account
        header: PageHeader {
            id: header2
            title: i18n.tr('Account Settings')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("Back")
								iconName:"back"
								onTriggered : {
									account.pageStack.removePages(account);
								}
							}
						]
					}
        }

        Column {
            id:mainarea2
            anchors {
                top: header2.bottom
                left: parent.left
                right: parent.right
                bottom: parent.top
                margins:units.gu(0.5)
            }
            spacing:units.gu(1)
            Label{
                id:status
                text: i18n.tr("...")
                Component.onCompleted:{
                    //determine status (loged in and online|offline|not logged in)
                    if(sets.number == "" && root.authkey == ""){
                        status.text = "Status: Not logged in";
                    }else{
                        if(root.authkey == ""){
                            status.text = "Status: Logged in";
                        }else{
                            status.text = "Status: Logged in; online";
                        }
                    }
                }
            }
            TextField {
			id:user
			property bool isLegitimate : true
			property bool isURL : false
			placeholderText: i18n.tr("username/Mobile number")
		}
		TextField {
			id:password
			property bool isLegitimate : true
			property bool isURL : false
			placeholderText: i18n.tr("password")
		}
		Button {
			id:commit
			width:units.gu(5)
			text : i18n.tr("login")
			iconName:  "ok"
            color: UbuntuColors.green
			onClicked : {
                var informed = false;
                ApiLog.logdata(sets.apikey, user.text, password.text, function(sucess, sheet){
                    if(sucess){
                        sets.number = user.text;
                        sets.pin = password.text;
                        root.authkey = sheet["user"]["loginkey"];
                        root.data = sheet;
                        if(!informed){
                        PopupUtils.open(yes);
                        informed = true;
                        }
                    }else{
                        if(!informed){
                        PopupUtils.open(no);
                        informed = true;
                        }
                    }
                 informed = true;   
            });
            }
		}
		Button {
			id:adelete
			width:units.gu(42)
			text : i18n.tr("LOG OUT")
            color: UbuntuColors.red
			onClicked : {
                sets.number = "";
                sets.pin = "";
                root.authkey = "";
                status.text = "Status: Not logged in";
            }
            }
        }
    }
        Page{
        //Account & settings
        anchors.fill: parent
        id:ainfo
        header: PageHeader {
            id: header4
            title: i18n.tr('Account information')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("Back")
								iconName:"back"
								onTriggered : {
									ainfo.pageStack.removePages(ainfo);
								}
							}
						]
					}
        }
        
        Column {
            anchors {
                top: header4.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins:units.gu(0.5)
            }
            spacing:units.gu(0.7)
    
        Row{
            Label{
                text:i18n.tr("Status: ")
            }
            Label{
                id:status1List
                text:i18n.tr("Logged in")
                
            }
            Label{
                id:status2List
                text:i18n.tr("NOT Logged in")
                
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("Mobile Number: ")
            }
            Label{
                id:numberList
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("User name/Mail: ")
            }
            Label{
                id:userList
            }
        }
    
    
    
        Row{
            Label{
                text:i18n.tr("Account active: ")
            }
            Label{
                id:activList
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("Credit: ")
            }
            Label{
                id:creditList
            }
        }
    
    
        Row{
            Label{
                text:i18n.tr("Free Minutes: ")
            }
            Label{
                id:minutesList
            }
        }
        Component.onCompleted:{
         if(root.authkey == "" && sets.number != ""){
                ApiLog.logdata(sets.apikey, sets.number, sets.pin, function(sucess, sheet){
                    if(sucess){
                        root.data=sheet;
                        root.authkey=sheet["user"]["loginkey"];
                        
                    
                        var usr = sheet["user"];
                        var act = "";
                        if(usr["active"]){act="YES";}else{act="NO"}
                        status1List.visible=true;
                        status2List.visible=false;
                        numberList.text=usr["mobile"];
                        userList.text = usr["screen_name"];
                        activList.text = act;
                        creditList.text = usr["credits"]+usr["currency"];
                        minutesList.text = usr["free_seconds"]/60;
                        
                    }
                });
            }else{
                    //in the first login numer+pwd aren't available. Showing the error message would irritate the user.
                    
                    if(root.data !={}){
                      var sheet=root.data;
                        root.authkey=sheet["user"]["loginkey"];
                        
                    
                        var usr = sheet["user"];
                        var act = "";
                        if(usr["active"]){act="YES";}else{act="NO"}
                        status1List.visible=true;
                        status2List.visible=false;
                        numberList.text=usr["mobile"];
                        userList.text = usr["screen_name"];
                        activList.text = act;
                        creditList.text = usr["credits"]+usr["currency"];
                        minutesList.text = usr["free_seconds"]/60;
                        
                      
                    }else{
                        status1List.visible=false;
                        status2List.visible=true;
                    }
        }
    }
        }

               
        }
        
        
    Page{
        

        //Account & settings
        anchors.fill: parent
        id:retdia
        header: PageHeader {
            id: header6
            title: i18n.tr('Return bike')
            leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("Back")
								iconName:"close"
								onTriggered : {
									retdia.pageStack.removePages(retdia);
								}
							}
						]
					}
        }
Component {
         id: retno
         Dialog {
             
             id: retdia
             title: i18n.tr("Failed")
             text: i18n.tr("No bike has been returned.\nPlease check if the bike already has been returned (+wait 15 seconds) and you are logged in correctly.")
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.orange
                 onClicked: PopupUtils.close(retdia)
             }
         }
    }
    Component {
         id: retyes
         Dialog {
             
             id: retdia
             title: i18n.tr("Bike returned")
             text: i18n.tr("The bike should now be returned.\nThank you for using the NUTbikes app.")
             Button {
                 text: i18n.tr("close")
                 color: UbuntuColors.green
                 onClicked: PopupUtils.close(retdia)
             }
         }
    }
        Column {
            id:mainarea4
            anchors {
                
                top: header6.bottom
                left: parent.left
                right: parent.right
                bottom: parent.top
                margins:units.gu(0.5)
                
            }
            spacing:units.gu(1)
            Label{
                
                text: i18n.tr("To return the bike (number: " +root.toreturn+"),\n"+text2.text)
            }
            TextField {
			id:statID
			placeholderText: i18n.tr("Station ID")
		}
		TextField {
			id:place
			placeholderText: i18n.tr("place of return")
		}
		Label{
            
                text: i18n.tr("\nIf there is something wrong\n(lock broken, dead battery, flat tire, ...) also fulfill this field.\nIf not, ignore")
            }
		TextField {
			id:comment
			placeholderText: i18n.tr("Comment")
		}
		Button {
			id:commit2
			text : "RETURN BIKE"
            color: UbuntuColors.green
			onClicked : {
                var reacted = false;
            ApiLog.retbike(sets.apikey, root.authkey, comment.text, place.text, statID.text, root.toreturn, function(sucess, sheet){
                
                if(sucess){
                    if(!reacted){
                    PopupUtils.open(retyes);
                    reacted = true;
                    }
                }else{
                    if(!reacted){
                    PopupUtils.open(retno);
                    reacted = true;
                    }
                }
            });
            }
		}
        }
    }

                }
                            function appendhelper(){
                
            bikes.clear()
            var collections = root.collections;
            if (collections.length > 0){
                mainarea.visible = false;
                mainbike.visible = true;
            }else{
                mainarea.visible = true;
                mainbike.visible = false;
            }
            for(var i = 0; i < collections.length; i++){
            var col = collections[i];
            var nr = col["bike"];
            
            var stempdif = Math.floor(Date.now()/1000-col["start_time"]);
            
            var h = Math.floor(stempdif/60/60);
            var m = Math.floor(stempdif%(60*60)/60);
            var s = Math.floor(stempdif-h*60*60-m*60);
            bikes.append({"bikenr":nr, "counter":text1.text+h+":"+m+":"+s});
            
            }
            
        
            }
                }
        

