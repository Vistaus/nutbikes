# NUTbikes

Inofficial Nextbike client for ubuntutouch in a NUT shell.
This Client uses the (proprietary but reverse engineered) API as described here:https://github.com/h0chi/nextbike-api-reverse-engineering


## What works?
- give-back bikes
- List feature (Duration + listed bikes updated every 15 seconds)
- Logo
- Adjusting API key throw web interface
- better display of the content (in columns)
- Logging in an existing Account
- Log out of the Account
- Show some Account details, read out of the API:
    - Mobile Number
    - Username (or e-mail adress)
    - Is the Account active or locked (Nextcloud locks the account e.g. if you have to much debts by driving without paying)
    - current credit
    - Free Minutes (you are able to get them by several actions, e.g. in Dresden bringing back bikes from remote to central points)
- Rent Bikes

### Are you Sure?!

Absolutly not. All the features are tested with my account and the provider **MOBI Bike in Dresden**. This app is not that old, so feel free to test it and give me a feedback. Honestly, __PLEASE__ do that! How else should I know if thoose features are working correctly everywhere...

## What isn't working? (TO-Do's)

- Show advanced properties of the bike like start point, type, user comment in an aditional column/Page
- Enable QR scanning : not that hard, as it sounds, we only need to register a URL in the URLDispatcher (https://nxtb.it), because the content of the QR's is https://nxtb.it/[bikenr]. If this is implemented the user just has to scan the code on tagger, touch on the open-url button which would open the NUTbikes app and unlock the bike.
- The official app suports group actions. Well, I am not familar with it, I even don't know if there has to be changed something, but if yes it is **not implemented yet**.

## Install

### Directly install Package

Download the file https://gitlab.com/S60W79/nutbikes/-/blob/main/nutbikes.s60w79_1.0.0_BETA.click on the device's browser.  
If you have not installed it yet, install the App UTtweak tool on the device (see: https://open-store.io/app/ut-tweak-tool.sverzegnassi)  
Open the tweek tool and go to -> System (in the menu on the left top) -> install Click package -> Choose -> (choose the downloaded file) -> Install

### With clickable

If you have installed clickable you may simply execute thoose steps on your linux PC, the device connected by USB. The development mode on the device should be enabled.

```
git clone https://gitlab.com/S60W79/nutbikes/

cd NUTbikes

clickable
```


## License

Copyright (C) 2021  s60w79

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
